//Sistema
import React, { Component } from "react";
import { createAppContainer, createStackNavigator } from "react-navigation";

//Redux
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import ReduxThunk from "redux-thunk";

//Reducers
import Reducers from "./src/Reducers";

//screens
import Home from "./src/routes/Home";
import Login from "./src/routes/Login";
import Cadastro from "./src/routes/Cadastro";

//Criando store
let store = createStore(Reducers, applyMiddleware(ReduxThunk));

const Navigation = createStackNavigator({
  Home: {
    screen: Home,
    title: Home,
    navigationOptions: {
      header: null
    }
  },
  Login: {
    screen: Login
  },
  Cadastro: {
    screen: Cadastro
  }
});

const StackNav = createAppContainer(Navigation);

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <StackNav />
      </Provider>
    );
  }
}
