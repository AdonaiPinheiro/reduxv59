const initialState = {
  email: "",
  senha: ""
};

const AuthReducer = (state = [], action) => {
  if (state.length == 0) {
    return initialState;
  }

  if (action.type == "editEmail") {
    return { ...state, email: action.payload.email };
  }

  if (action.type == "editSenha") {
    return { ...state, senha: action.payload.senha };
  }

  if (action.type == "cadastrar") {
    alert("Cadastrando");
  }

  if (action.type == "cadastroSucesso") {
    alert("Cadastro com sucesso");
    return state;
  }

  if (action.type == "cadastroErro") {
    alert("Erro " + action.payload.code);
    return state;
  }

  return state;
};

export default AuthReducer;
