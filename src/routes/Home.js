import React, { Component } from 'react';
import { Text, StyleSheet, View, Button } from 'react-native';

export default class Home extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={{marginBottom:5}}> Home </Text>

        <View style={{ flexDirection:'row', justifyContent:'space-evenly' }}>
          
          <Button title="Cadastrar" onPress={()=>this.props.navigation.navigate("Cadastro")} />
          <Button title="Login" onPress={()=>this.props.navigation.navigate("Login")} />
        </View>
        
      </View>
    )
  }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    }
});
