import React, { Component } from "react";
import { Text, StyleSheet, View, TextInput, Button } from "react-native";
import { connect } from "react-redux";

import { editEmail } from "../actions/AuthActions";

export class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.entrar = this.entrar.bind(this);
  }

  entrar() {}

  render() {
    return (
      <View style={styles.container}>
        <Text> Login </Text>

        <TextInput
          style={{ margin: 5, borderColor: "#000", borderWidth: 1 }}
          placeholder="Usuário"
          value={this.props.email}
          onChangeText={txt => {
            this.props.editEmail(txt);
          }}
        />

        <TextInput
          secureTextEntry={true}
          style={{ margin: 5, borderColor: "#000", borderWidth: 1 }}
          placeholder="Senha"
          value={this.props.senha}
        />

        <Button title="Logar" onPress={this.entrar} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

const mapStateToProps = state => {
  return {
    email: state.auth.email,
    senha: state.auth.senha
  };
};

const LoginConnect = connect(
  mapStateToProps,
  { editEmail }
)(Login);

export default LoginConnect;
