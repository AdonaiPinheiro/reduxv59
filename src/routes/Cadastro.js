import React, { Component } from "react";
import { Text, StyleSheet, View, Button, TextInput } from "react-native";
import { connect } from "react-redux";

//Actions
import { editEmail, editSenha, cadastrar } from "../actions/AuthActions";

export class Cadastro extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (

        <View>
          <Text> Cadastrar </Text>

          <TextInput
            style={{ margin: 5, borderColor: "#000", borderWidth: 1 }}
            placeholder="Usuário"
            value={this.props.email}
            onChangeText={email => this.props.editEmail(email)}
          />

          <TextInput
            secureTextEntry={true}
            style={{ margin: 5, borderColor: "#000", borderWidth: 1 }}
            placeholder="Senha"
            value={this.props.senha}
            onChangeText={senha => this.props.editSenha(senha)}
          />

          <Button
            title="Cadastrar"
            onPress={() => {
              this.props.cadastrar(this.props.email, this.props.senha);
            }}
          />

          <Button
            title="Toast"
            onPress={() =>
                Toast.show({
                  text: "Wrong password!",
                  buttonText: "Okay",
                  duration: 3000
                })}
          />
        </View>
      
    );
  }
}

const styles = StyleSheet.create({});

const mapStateToProps = state => {
  return {
    email: state.auth.email,
    senha: state.auth.senha
  };
};

const CadastroConnect = connect(
  mapStateToProps,
  { editEmail, editSenha, cadastrar }
)(Cadastro);

export default CadastroConnect;
